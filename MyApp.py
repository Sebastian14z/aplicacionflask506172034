import mysql.connector
import pandas as pd
import numpy as np

from geopy.geocoders import Nominatim
from geopy import distance
from mysql.connector import Error
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error
from sklearn.tree import DecisionTreeRegressor
from flask import Flask, render_template, request, redirect

##############
#     DB     #
##############
host = "baitybait.caxxs8jtarvs.us-east-2.rds.amazonaws.com"
usuario = "admin"
contrasenia = "Sebastian1028$"
basedatos = "sebastian"
conexion = None
try:
    conexion = mysql.connector.connect(
        host=host, user=usuario, passwd=contrasenia, database=basedatos
    )
    print("Correct conection")
except Error as e:
    print("Error: ", e)


##################
#     MODELS     #
##################
data = pd.read_csv("deliverytime.txt")
geoApp = Nominatim(user_agent="XAZC")
food_model = None


def distCalculateLatLon(lat1, lon1, lat2, lon2):
    latitud_inicial = lat1
    longitud_inicial = lon1
    posicion_inicial = (latitud_inicial, longitud_inicial)

    latitud_final = lat2
    longitud_final = lon2
    posicion_final = (latitud_final, longitud_final)

    return round(distance.distance(posicion_inicial, posicion_final).km, 4)


def initDistanceModel():
    data["distance"] = np.nan

    for i in range(len(data)):
        data.loc[i, "distance"] = distCalculateLatLon(
            data.loc[i, "Restaurant_latitude"],
            data.loc[i, "Restaurant_longitude"],
            data.loc[i, "Delivery_location_latitude"],
            data.loc[i, "Delivery_location_longitude"],
        )


def defineDir(dir):
    try:
        definedDir = geoApp.geocode(dir)
        if (definedDir is None):
            return False
        return {
            "lat": definedDir.latitude,
            "lon": definedDir.longitude,
        }
    except Error as e:
        print(e)
        return False


initDistanceModel()

x = data[["Delivery_person_Age", "Delivery_person_Ratings", "distance"]]
y = data[["Time_taken(min)"]]
train_X, val_X, train_y, val_y = train_test_split(x, y, random_state=0)
# Define model
food_model = DecisionTreeRegressor(random_state=20)
# Fit model
food_model.fit(train_X, train_y)


###############################
#     FUNCIONES GENERALES     #
###############################
def consultar(sql):
    if conexion != None:
        cursor = conexion.cursor(dictionary=True)
        try:
            cursor.execute(sql)
            return cursor.fetchall()
        except Error as e:
            print("Error: ", e)
    else:
        print("No existe conexión a la base de datos")


def inserta(sql):
    if conexion != None:
        try:
            print(sql)
            cursor = conexion.cursor()
            cursor.execute(sql)
            conexion.commit()
        except Error as e:
            print("Error: ", e)
    else:
        print("No existe conexión a la base de datos")


#########################
#     FUNCIONES APP     #
#########################
def consultarOperaciones():
    sql = "select * from operations order by date desc"
    return consultar(sql)


def insertarOperacion(age, rating, origin, target, distance, time):
    sql = (
        "insert into operations (agePartner, rating, origin, target, totalDistance, timePredicted) values ("
        + str(age)
        + ", "
        + str(rating)
        + ", '"
        + origin
        + "', '"
        + target
        + "', "
        + str(distance)
        + ", "
        + str(time)
        + ")"
    )
    inserta(sql)


def calcularTiempo(edad, calificacion, origen, destino):
    dir1 = defineDir(origen)
    dir2 = defineDir(destino)
    if dir1 == False:
        return {"error": "Direccion de origen invalida"}
    if dir2 == False:
        return {"error": "Direccion de destino invalida"}

    distancia = distCalculateLatLon(dir1["lat"], dir1["lon"], dir2["lat"], dir2["lon"])

    nuevo_pedido = pd.DataFrame(
        columns=["Delivery_person_Age", "Delivery_person_Ratings", "distance"]
    )
    nuevo_pedido["Delivery_person_Age"] = [int(edad)]
    nuevo_pedido["Delivery_person_Ratings"] = [float(calificacion)]
    nuevo_pedido["distance"] = [int(distancia)]
    return {"time": food_model.predict(nuevo_pedido)[0], "distance": distancia}


#################
#     FLASK     #
#################
app = Flask(__name__)


@app.route("/")
def index():
    return redirect("/inicio", code=302)


@app.route("/inicio")
def inicio():
    response = {"error": False, "operations": consultarOperaciones()}
    return render_template("inicio.html", response=response)


@app.route("/evaluar", methods=["GET", "POST"])
def registrar():
    error = False
    edad = request.form["age"]
    calificacion = request.form["rate"]
    origen = request.form["origin"]
    destino = request.form["target"]
    pedido = calcularTiempo(edad, calificacion, origen, destino)
    if "error" in pedido:
        return render_template("inicio.html", response={"error": pedido["error"]})
    insertarOperacion(
        edad, calificacion, origen, destino, pedido["distance"], pedido["time"]
    )
    response = {"error": False, "operations": consultarOperaciones()}
    return render_template("inicio.html", response=response)


if __name__ == "__main__":
    data.tail()
    app.run(host="0.0.0.0", port=5000, debug=True)
